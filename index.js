first_name = "John";
firstName = "First Name: " + first_name;
console.log(firstName);

last_name = "Smith";
let lastName = "Last Name: " + last_name;
console.log(lastName);

let age = 30;
console.log("Age: " + age);

console.log("Hobbies:");
let hobbies = ["Gaming", "Reading books", "Music"];
console.log(hobbies);

console.log("Work Address:");
let workAddress = {
  houseNumber: "32",
  street: "Washington",
  city: "Lincoln",
  state: "Nebraska",
};
console.log(workAddress);

console.log("My full name is: " + first_name + " " + last_name);
console.log("My current age is: " + age);

console.log("My Friends are:");
let friends = ["James", "Hanna", "Edward", "Bryan", "Donna"];
console.log(friends);

console.log("My Full Profile:");
let profile = {
  username: "captain_america",
  fullName: "Steve Rogers",
  age: 40,
  isActive: false,
};
console.log(profile);

best_friend = "Bucky Barnes";
console.log("My Bestfriend is: " + best_friend);

place = "Arctic Ocean";
console.log("I was found frozen in: " + place);
